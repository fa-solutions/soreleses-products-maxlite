let FAClient = null;
let recordId = null;
let linesGlobal = null;
let productList = null;
let deleteLinesGlobal = [];
let recordGlobal = null;

let repId = null;

let keyGlobal = "";
let baseUrlGlobal = "";

let executingRequest = false;

let notyf = null;

let timer = 0;
let countDownId = null;

let cleanQuoteGlobals = () => {
    recordId = null;
    linesGlobal = null;
    productList = null;
    repId = null;
}
// aHR0cHM6Ly9kNmY3LTI4MDYtMmYwLTUxYzEtMTcyYy01OWZkLTVlYzEtYzgxNS01NWM3Lm5ncm9rLmlv 
// aHR0cDovL2xvY2FsaG9zdDo1MDAw localhost:5000
//aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL3NvcmVsZXNlcy1wcm9kdWN0cy1tYXhsaXRl // production https://fa-solutions.gitlab.io/soreleses-products-maxlite
//a la hora de agregar ocurre lo siguiente, se valida que el item no exista y si existe se updatea, no se crea uno nuevo, ademas de que una vez generado
//el nuevo item se  bloquean los botones de agregar

const SERVICE = {
    name: 'FreeAgentService',
    appletId: `aHR0cHM6Ly9mYS1zb2x1dGlvbnMuZ2l0bGFiLmlvL3NvcmVsZXNlcy1wcm9kdWN0cy1tYXhsaXRl`,
};

let config = {
    opp: {
        name: 'deal',
        ID: 'seq_id',
        jobName: 'deal_field5',
        repAgency: 'deal_field13',
        amount: 'amount',
        targetDate: 'close_date',
        salesStage: 'sales_stage_id',
        status: 'status_id',
        winProbability: 'deal_field2',
        owner: 'owner_id',
        createdBy: 'created_by',
        createAt: 'created_at'
    },
    poLines: {
        name: "po_release",
        fields: {
            soRecordRef: 'po_release_field1'
        }
    },
    lines: {
        name: 'quote_item',
        fields: {
            itemNumber: 'quote_item_field0',
            itemDescription: 'quote_item_field2',
            descriptionSpecial: 'description',
            itemType: 'quote_item_field7',
            competitorName: 'quote_item_field8',
            price: 'quote_item_field11',
            lineAmount: 'quote_item_field12',
            commissionAmount: 'quote_item_field13',
            quantity: 'quote_item_field14',
            commissionPercent: 'quote_item_field33',
            commissionPercentOverwrite: 'quote_item_field18',
            itemCategory: 'quote_item_field19',
            available: 'quote_item_field20',
            status: 'quote_item_field34',
            wareHouse: 'quote_item_field21',
            productName: 'quote_item_field22',
            alternativeTo: 'quote_item_field23',
            typeText: 'quote_item_field24',
            releaseAmount: 'quote_item_field25',
            order: 'quote_item_field29',
            aPrice: "quote_item_field30",
            hotPrice: "quote_item_field31",
            lifeCycle: "quote_item_field32",
        }
    },
    products: {
        name: 'product',
        fields: {
            descriptionName: 'product_field1',
            description: 'description',
            itemNumber: 'product_field0',
            aPrice: 'product_field2',
            hotPrice: 'product_field3',
            orderCode: 'product_field4',
            commissionPercent: 'product_field5',
            active: 'product_field6',
            mainCategory: 'product_field7',
            subCategory: 'product_field8',
            lifeCycle: 'product_field9',
            onHand: 'product_field11',
            available: 'product_field12',
            allocated: 'product_field13'
        }
    },
    parent: {
        name: 'quote',
        fields: {
            quoteStage: "quote_field37",
            quoteStatus: "quote_field1",
            expireDate: "quote_field16",
            expireNotificationDate: "quote_field71",
            reminderDate: "quote_field3",
            targetDate: "quote_field50"
        }
    }
}

function startupService() {
    console.log('STARTUP SERVICE TEST 2');
    notyf = new Notyf({
        duration: 20000,
        dismissible: true,
        position: {
            x: 'center',
            y: 'bottom',
        },
        types: [
            {
                type: 'info',
                className: 'info-notyf',
                icon: false,
            },
        ],
    });

    FAClient = new FAAppletClient({
        appletId: SERVICE.appletId,
    });

    const countDown = () => {
        timer -= 1;
        document.getElementById("timerValue").innerHTML = `${timer} s`;

        if (timer == 0 || timer < 0) {
            clearInterval(countDownId);
            countDownId = null;
            let activeQuoteTextEl = document.getElementById('record-info-text');
            let cleanActiveRecordButton = document.getElementById('remove-active-quote');

            activeQuoteTextEl.textContent = `Quote is not selected`;
            cleanQuoteGlobals();
            cleanList();
            cleanActiveRecordButton.style.display = `none`;
            searchEl.value = '';
            activeQuoteTextEl.classList.remove('is-link');
            FAClient.close();
        }
    };

    let searchEl = document.getElementById('search-input');
    let searchSelectEl = document.getElementById('search-select');
    let submitSo = document.getElementById('submit-so-button');
    let reorderButton = document.getElementById('reorder-lines-button');
    let bulkDeleteButton = document.getElementById('delete-lines-button');
    let cancelButton = document.getElementById('cancel-delete-lines-button');
    let downloadButton = document.getElementById('download-quote-button');

    
    cancelButton.addEventListener('click', async (e) => {
        cleanList();
        await generateList({ products: null, isChange: true })
    })

    reorderButton.addEventListener('click', (e) => {
        let reorderText = document.getElementById('reorder-button-text');
        if (reorderButton?.innerText == 'Reorder') {
            renderedReorderLines(linesGlobal);
            //reorderText.innerText = 'Save';
            reorderButton.innerText = 'Save';
            cancelButton.style.display = 'flex';
            bulkDeleteButton.style.display = 'none';
        } else {
            reorderButton.innerText = 'Reorder';
            cancelButton.style.display = 'none';
            saveReorderedLine().then(res => null).catch(e => console.log(e));
        }
    });

    bulkDeleteButton.addEventListener('click', async (e) => {
        if (bulkDeleteButton?.innerText === 'Bulk Delete') {
            reorderButton.style.display = 'none';
            bulkDeleteButton.innerText = "Delete";
            renderedDeleteLines(linesGlobal);
            cancelButton.style.display = 'flex';
        } else {
            await bulkDeleteLines().catch(e => console.log(e));
            bulkDeleteButton.innerText = "Bulk Delete";
            reorderButton.style.display = 'flex';
        }
    });

    downloadButton.addEventListener('click', async (e) => {
        downLoadQuoteCsv()
    })

    keyGlobal = FAClient.params.key;
    baseUrlGlobal = FAClient.params.baseUrl;

    FAClient.on("openMarginModal", async ({ record, redirect = false }) => {

        let list = document.querySelector('#order-list');
        
        if (countDownId) {
            clearInterval(countDownId);
            countDownId = null;
        }
        timer = 300;
        countDownId = setInterval(countDown, 1000);
        if (record && record.seq_id) {
            recordGlobal = record;
            recordId = record.id;
            repId = record?.field_values?.quote_field54?.value;
            let activeQuoteTextEl = document.getElementById('record-info-text');
            activeQuoteTextEl.innerText = `Quote# ${record.seq_id}`;
            activeQuoteTextEl.addEventListener('click', () => {
                if (recordId) {
                    FAClient.navigateTo(`/quote/view/${recordId}`)
                }
            });

            activeQuoteTextEl.classList.add('is-link');
            let cleanActiveRecordButton = document.getElementById('remove-active-quote');
            cleanActiveRecordButton.style.display = `block`;
            cleanActiveRecordButton.addEventListener('click', (e) => {
                activeQuoteTextEl.textContent = `Quote is not selected`;
                cleanQuoteGlobals();
                cleanList();
                cleanActiveRecordButton.style.display = `none`;
                searchEl.value = '';
                activeQuoteTextEl.classList.remove('is-link');
            });

            if (redirect && recordId) {
                FAClient.navigateTo(`/quote/view/${recordId}`);
            }
            

            FAClient.open();
            await openProducts().catch(e => console.log(e));
            list.innerHTML = `<div style="padding: 100px"><h1 style="color: white">Downloading....</h1></div>`
            let quoteItems = linesGlobal.map( r => { 
                return {
                    
                    id : r.id,
                    item : !!r.field_values.quote_item_field0.display_value ? r.field_values.quote_item_field0.display_value : null,
                    quantity : !!r.field_values.quote_item_field14.value ? r.field_values.quote_item_field14.value : 0,
                    lineTotal : !!r.field_values.quote_item_field12.value ? r.field_values.quote_item_field12.value : 0,
                    description : !!r.field_values.quote_item_field2.value ? r.field_values.quote_item_field2.value : "",
                    price : !!r.field_values.quote_item_field11.value ? r.field_values.quote_item_field11.value : 0,
                    commission: !!r.field_values.quote_item_field18.value ? r.field_values.quote_item_field18.value : "",
                    commissionAmount: !!r.field_values.quote_item_field13.value ? r.field_values.quote_item_field13.value : 0,
                    lifeCycle: !!r.field_values.quote_item_field32.display_value ? r.field_values.quote_item_field32.display_value : '',
                    primary: r.field_values.quote_item_field7.value,
                    type: !!r.field_values.quote_item_field24.display_value ? r.field_values.quote_item_field24.display_value : '',
                }
            })
            let promises = []
            quoteItems.map((item) => {
           
               promises.push(getMargin(item))
            
            })
            const marginLines = await Promise.all(promises);

            handleSum(marginLines);

            let data = Papa.unparse(marginLines)
            console.log('data',data,recordGlobal.seq_id)
           
            let csvData = new Blob([data]);
            const a = document.createElement('a');
            a.href = URL.createObjectURL(csvData, { type: 'text/csv;charset=utf-8;' });
            a.download = `${recordGlobal.seq_id}-quote-items.csv`;
            encodedUri = encodeURI(csvData)
            document.body.appendChild(a);
            a.click();
            document.body.removeChild(a);
            list.innerHTML = `<div style="padding: 100px"><h1 style="color: white">Complete!</h1></div>`
        }

    })

    FAClient.on("openMaxLiteProducts", async ({ record, redirect = false }) => {
        if (countDownId) {
            clearInterval(countDownId);
            countDownId = null;
        }
        timer = 300;
        countDownId = setInterval(countDown, 1000);

        const searchContainer = document.querySelector('.search-container')
        const footer = document.querySelector('.footer')
        const orderList = document.getElementById('order-list')
        searchContainer.style = ""
        footer.style = ""
        orderList.style = "padding: 10px; margin: 0"

        const oppContainer = document.getElementById('opp-container')
        oppContainer.style = "display: none"
        if (searchEl) {
            searchEl.value = '';
        }
        if (searchSelectEl) {
            searchSelectEl.value = 'Search All';
        }

        if (record && record.seq_id) {
            recordGlobal = record;

            recordId = record.id;
            repId = record?.field_values?.quote_field54?.value;
            let activeQuoteTextEl = document.getElementById('record-info-text');
            activeQuoteTextEl.innerText = `Quote# ${record.seq_id}`;
            activeQuoteTextEl.addEventListener('click', () => {
                if (recordId) {
                    FAClient.navigateTo(`/quote/view/${recordId}`)
                }
            });

            activeQuoteTextEl.classList.add('is-link');
            let cleanActiveRecordButton = document.getElementById('remove-active-quote');
            cleanActiveRecordButton.style.display = `block`;
            cleanActiveRecordButton.addEventListener('click', (e) => {
                activeQuoteTextEl.textContent = `Quote is not selected`;
                cleanQuoteGlobals();
                cleanList();
                cleanActiveRecordButton.style.display = `none`;
                searchEl.value = '';
                activeQuoteTextEl.classList.remove('is-link');
            });

            if (redirect && recordId) {
                FAClient.navigateTo(`/quote/view/${recordId}`);
            }

            FAClient.open();
            await openProducts().catch(e => console.log(e));
        }
    });

    FAClient.on("openPushTargetDate", async ({ record, redirect = false }) => {
        if (countDownId) {
            clearInterval(countDownId);
            countDownId = null;
        }
        timer = 300;
        countDownId = setInterval(countDown, 1000);
        const searchContainer = document.querySelector('.search-container')
        const footer = document.querySelector('.footer')
        const orderList = document.getElementById('order-list')
        searchContainer.style.display = 'none'
        footer.style.display = 'none'
        orderList.style.display = 'none'

        const oppContainer = document.getElementById('opp-container')
        oppContainer.style = "padding: 20px;background: rgba(69, 86, 96, 1.00);margin: 20px;"
        if (record && record.seq_id) {
            recordGlobal = record;
            recordId = record.id;
            
            let activeQuoteTextEl = document.getElementById('record-info-text');
            activeQuoteTextEl.innerText = `OPP# ${record.seq_id}`;
            activeQuoteTextEl.addEventListener('click', () => {
                if (recordId) {
                    FAClient.navigateTo(`/deal/view/${recordId}`)
                }
            });

            activeQuoteTextEl.classList.add('is-link');
            let cleanActiveRecordButton = document.getElementById('remove-active-quote');
            cleanActiveRecordButton.style.display = `block`;
            cleanActiveRecordButton.addEventListener('click', (e) => {
                activeQuoteTextEl.textContent = `Quote is not selected`;
                cleanQuoteGlobals();
                cleanList();
                cleanActiveRecordButton.style.display = `none`;
                searchEl.value = '';
                activeQuoteTextEl.classList.remove('is-link');
            });

            if (redirect && recordId) {
                FAClient.navigateTo(`/deal/view/${recordId}`);
            }

            
            oppContainer.innerHTML = `
            <label style="color: white" for="start">Target Date:</label>

            <input type="date" id="target-date" name="Target Date"
                   value="${new Date().toLocaleDateString()}"
                   >
            <br>
            <br>
            <label style="color: white"  for="start">Expiration Date:</label>

            <input type="date" id="exp-date" name="Expiration Date"
                    value="${new Date().toLocaleDateString()}"
                   >
            <br>
            <br>
            <button id="target-date-push-btn" style="margin: 0 auto">Submit</button>
            `
            targetButton = document.getElementById('target-date-push-btn')

            targetButton.addEventListener('click', async (ev) =>  {
                const targetDateEl = document.getElementById('target-date')
                const expireDateEl = document.getElementById('exp-date')

                await pushTargetDate(targetDateEl.value, expireDateEl.value)
            })
            FAClient.open();
            
        }
    });
        

    
    FAClient.on("openStatingProducts", async ({ record }) => {
        searchEl.value = '';
        await openProducts(record, searchSelectEl).catch(e => console.log(e));
    });

    searchEl.addEventListener("keyup", debounce(async (event) => {
        if (event?.target?.value?.length > 2) {
            if (recordId) {
                await generateList({ isChange: true })
            } else {
                notyf.alert(`Please use "Add Products" button in Quotes App to open the applet`);
            }
        } else {
            cleanList();
        }
    }, 1000));

    searchSelectEl.addEventListener("change", async (event) => {
        if (recordId) {
            if (event.target.value === 'Included') {
                await generateList({ products: null, isChange: true })
            } else if (event.target.value === 'Upload') {
                await generateUploadUI();
            } else if (event.target.value === 'Alternative') {
                await generateList({ products: null, isChange: true });
            } else {
                cleanList();
            }
        } else {
            notyf.alert(`Please use "Add Products" button in Quotes App to open the applet`);
            event.target.value = 'Search All';
        }
    });

}

async function getRelatedQuotes(oppId) {
    return await FAClient.listEntityValues({
        entity: config.parent.name,
        filters: [
            {
                field_name: "quote_field27",
                operator: "equals",
                values: [oppId],
            },
        ],
        limit: 5
    });
}
async function pushTargetDate(targetDate, expireDate) {
    loadingStart()
    const relatedQuotes = await getRelatedQuotes(recordId)
    console.log(relatedQuotes)
    const promises = []
    if(relatedQuotes && relatedQuotes.length) {
        const quoteIds = relatedQuotes.map((quote => {
            return quote.id
        }))
        const reminderDate = new Date(targetDate)
        reminderDate.setDate(reminderDate.getDate() - 5)
        console.log("reminderDate", reminderDate)
        const quotePayload = {
            entity: config.parent.name,
            id: quoteIds[0],
            field_values: {
                [config.parent.fields.quoteStatus]: '8cf7f3dc-da9b-4741-846c-d696177776a0', //active
                [config.parent.fields.expireDate]: expireDate,
                [config.parent.fields.targetDate]: targetDate,
                [config.parent.fields.reminderDate]: reminderDate.toISOString()
            }
        }
        promises.push(FAClient.updateEntity(quotePayload))
    }
    
    const oppPayload = {
        entity: config.opp.name,
        id: recordId,
        field_values: { 
            [config.opp.targetDate]: targetDate
        }
    }
    
    
    promises.push(FAClient.updateEntity(oppPayload))
    
    await Promise.all(promises)
    loadingEnd()
}
function cleanList() {
    let reorderButton = document.getElementById('reorder-lines-button');
    reorderButton.style.display = "none";
    reorderButton.innerText = "Reorder";
    document.getElementById('cancel-delete-lines-button').style.display = "none";
    let deleteLinesButton = document.getElementById('delete-lines-button');
    deleteLinesButton.style.display = "none";
    deleteLinesButton.innerText = "Bulk Delete";
    let list = document.getElementById('order-list');
    if (list) {
        list.innerHTML = '';
    }
}

function debounce(callback, wait = 500) {
    let timerId;
    return (...args) => {
        clearTimeout(timerId);
        timerId = setTimeout(() => {
            callback(...args);
        }, wait);
    };
}

async function openProducts() {
    cleanList();
    linesGlobal = await listLineItems();
    FAClient.open();
    return null;
}


async function getIncludedProducts() {
    let lines = [];
    let linesObj = {}
    if (linesGlobal) {
        lines = linesGlobal;
        //lines = await listLineItems();
    } else {
        lines = await listLineItems();
    }

    let includedProducts = [];
    let includedProductsIds = [];
    let seqIds = []
    lines.map(line => {
        includedProductsIds.push(line?.field_values?.quote_item_field0?.value)
        includedProducts.push(line?.field_values?.quote_item_field0?.display_value)
        seqIds.push(line?.seq_id?.value)
    });

    let productsInLines = await FAClient.listEntityValues({
        entity: config.products.name,
        filters: [
            {
                field_name: config.products.fields.itemNumber,
                operator: "equals",
                values: includedProducts,

            }
        ],
        limit: 1000
    });
    let sortedProducts = [];

    includedProductsIds.map(prodId => {
        let prodFound = productsInLines.find(prod => prodId === prod.id);
        if (prodFound) {
            sortedProducts.push(prodFound);
        }
    })

    // productsInLines.filter(prod => includedProductsIds.includes(prod.id));
    return sortedProducts;
}

async function getCompetitorOptions() {
    return FAClient.listEntityValues({
        entity: 'catalog',
        fields: ['id', 'name'],
        filters: [
            {
                field_name: "parent_entity_reference_id",
                values: ["3eef4633-2fab-4794-baf7-f9bdbce8185e"]
            }
        ]
    })
}

async function getFilteredProducts(searchText) {
    return await FAClient.listEntityValues({
        entity: config.products.name,
        filters: [{
            field_name: 'product_field0',
            operator: 'contains',
            values: [searchText]
        }],
        order: [[config.products.fields.itemNumber, "ASC"]]
    });
}

function isNumeric(str) {
    if (typeof str != "string") return false // we only process strings!  
    return !isNaN(str) && // use type coercion to parse the _entirety_ of the string (`parseFloat` alone does not do this)...
           !isNaN(parseFloat(str)) // ...and ensure strings of whitespace fail
}

async function getFilteredProductsByOrderNo(searchText) {
    return await FAClient.listEntityValues({
        entity: config.products.name,
        filters: [{
            field_name: 'product_field19',
            operator: 'starts with',
            values: [searchText]
        }],
        order: [[config.products.fields.itemNumber, "ASC"]]
    });
}


async function getAlternativeProducts(itemNo) {
    let url = `${baseUrlGlobal}/alt-items?itemNo=${itemNo}`;
    headers['MX-Api-Key'] = keyGlobal;
    let response = await fetch(url, {
        method: 'GET',
        headers,
    });

    if (!(response && response.ok)) {
        notyf.alert(`Failed to get alternative items from SM100`);
        return null;
    }

    const alternativeItems = await response.json();

    try {
        const alternativeProducts = await FAClient.listEntityValues({
            entity: config.products.name,
            filters: [
                {
                    field_name: config.products.fields.itemNumber,
                    operator: "equals",
                    values: alternativeItems.map(alternativeItem => alternativeItem.itemNo),
                    // values: ["LS-4823U-50", "LS-4823U-50MS", "LS-4823U-40", "LS-4823U-40MS", "LS-4846U-40", "LS-4846U-40MS", "LS-4846U-50", "LS-4846U-50MS"]
                },
            ],
        });

        return alternativeProducts;
    } catch (error) {
        console.log(error);
        return null;
    }
}

async function generateList({ products = productList, lines = linesGlobal, isChange = false }) {
    cleanList();
    let bodyContainer = document.querySelector('body');
    let list = document.querySelector('#order-list');
    let searchEl = document.getElementById('search-input');
    let searchSelectEl = document.getElementById('search-select');
    let searchText = searchEl.value && searchEl.value.trim(' ') !== '' ? searchEl.value.trim(' ') : searchEl.value;
    let selectText = searchSelectEl?.value;
    let productsToDisplay = null;
    let competitorOptions = [];
    //if select text is included this get all the line items that the record has
    if (selectText && selectText === 'Included') {
        document.getElementById('reorder-lines-button').style.display = "flex";
        document.getElementById('delete-lines-button').style.display = "flex";
        products = await getIncludedProducts().catch(e => console.log(e));
        competitorOptions = await getCompetitorOptions();
    } else {
        document.getElementById('reorder-lines-button').style.display = "none";
    }

    if (selectText && selectText === 'Alternative') {
        products = await getAlternativeProducts(searchText).catch(e => console.log(e));
        competitorOptions = await getCompetitorOptions();
    }

    if (selectText === 'Search All' && isChange) {
        loadingStart()
        if(isNumeric(searchText)) {
            products = await getFilteredProductsByOrderNo(searchText).catch(e => console.log(e));
        } else {
            products = await getFilteredProducts(searchText).catch(e => console.log(e));
        }
        loadingEnd()
        competitorOptions = await getCompetitorOptions();
    }


    let subCategories = {};

    products?.map((product, index) => {
        let lineMatch = null;

        if (lines[index] && lines[index].field_values[config.lines.fields.itemNumber].value == product.id) {
            lineMatch = lines[index];
        }

        let lineId = lineMatch?.id;
        let qty = lineMatch?.field_values[config.lines.fields.quantity]?.value || '';
        let price = lineMatch?.field_values[config.lines.fields.price]?.value || '';
        let co = lineMatch?.field_values[config.lines.fields.commissionPercentOverwrite]?.value || '';
        let whLine = lineMatch?.field_values[config.lines.fields.wareHouse]?.value || '';
        let itemAlternativeType = lineMatch?.field_values[config.lines.fields.itemType]?.value;
        let alternateToLine = lineMatch?.field_values[config.lines.fields.alternativeTo]?.value || null;
        let typeText = lineMatch?.field_values[config.lines.fields.typeText]?.value || '';
        let specialDescriptionLine = lineMatch?.field_values[config.lines.fields.descriptionSpecial]?.value || null;
        let competitorName = lineMatch?.field_values[config.lines.fields.competitorName]?.display_value || '';

        let fieldValues = product.field_values;

        let productName = fieldValues[config.products.fields.descriptionName].display_value;
        let itemNumber = fieldValues[config.products.fields.itemNumber].display_value;
        let productDescription = fieldValues[config.products.fields.description].display_value;
        let mainCategory = fieldValues[config.products.fields.mainCategory].display_value;
        let subCategory = fieldValues[config.products.fields.subCategory].display_value;
        let commissionPercent = fieldValues[config.products.fields.commissionPercent].formatted_value;

        subCategories[subCategory] = fieldValues[config.products.fields.mainCategory].display_value;

        let liEl = document.createElement('li');
        liEl.setAttribute('id', `liEl${index}`);
        liEl.setAttribute('data-id', `${product.id}`);
        liEl.setAttribute('data-index', `${index}`);
        if (lineId) {
            liEl.setAttribute('data-lineid', `${lineId}`);
        }
        if (qty > 0) {
            liEl.classList.add('included-in-lines');
        }

        let hasItems = qty && qty > 0 && price && price > 0;
        let productInfoContainer = document.createElement('div');

        const addButtonHtml = (styles = '') => {
            return `<div style="${styles}" id="addButton${index}" class="update-line-button">
                        <button>Add</button>
                    </div>`;
        };

        const alternativeButtonHtml = (styles = '') => {
            return `<div style="${styles}" id="alternativeButton${index}" class="update-line-button">
                        <button>Alternative</button>
                    </div>`
        };

        /* if (!hasItems && itemNumber && itemNumber[0] && itemNumber[0] === '#') {
            addButtonHtml = `<div id="addButton${index}" class="update-line-button">
                                <button>Add</button>
                            </div>`
        } */

        const updateButtonHtml = (styles = '') => {
            return `<div styles="${styles}" id="updateButton${index}" class="update-line-button">
                        <button>Update</button>
                    </div>`
        };

        const removeButtonHtml = (styles = '') => {
            return `<div style="${styles}" id="removeButton${index}" class="update-line-button">
                        <button>Remove</button>
                    </div>`;
        }

        let checkBox = `<div id="addButton${index}" class="update-line-button">
            <input id="checkbox${index}" type="checkbox" checked />
        </div>`

        function renderedButton() {
            let toRender = '';
            if (hasItems) {
                toRender = `${removeButtonHtml("display:" + hasItems ? "flex" : "none")}
                            ${updateButtonHtml("display: flex")}
                            ${alternativeButtonHtml("display: flex")}
                            ${addButtonHtml("display: flex")}`
            } else {
                if (itemNumber.startsWith('#', 0)) {
                    toRender = `
                            ${alternativeButtonHtml()}
                            ${addButtonHtml()}
                            ${updateButtonHtml("display: none")}
                            ${removeButtonHtml("display: none")}`
                } else {
                    toRender = `
                            ${alternativeButtonHtml()}
                            ${addButtonHtml()}
                            ${updateButtonHtml("display: none")}
                            ${removeButtonHtml("display: none")}`
                }
            }
            return toRender;
        }

        productInfoContainer.innerHTML = `<div>
        <div class="row header-paragraph">
            <div class="row">
                <button id="collapseButton${index}"
                        class="dropdown-toggle"
                        style="color: white; font-size: 13px; background: transparent; border: none" type="button" data-bs-toggle="collapse" data-bs-target="#multiCollapseExample${index}" aria-expanded="false" aria-controls="multiCollapseExample${index}">
                    <b style="white-space: pre-line">
                        <span><b><span style="color: rgba(215, 217, 219, 1.00);padding: 5px;background:green">${itemNumber}</span></b> ${price ? `<span id="meta-${itemNumber}">Price: $${price},  Qty: ${qty}</span>` : ''} </span>
                        <span  style="color: rgba(215, 217, 219, .80);">${productName}</span>
                    </b>
                </button>
            </div>
        </div>
        <div class="collapse multi-collapse info-container" id="multiCollapseExample${index}">
            <div class="info-container-description-row">
                <textarea placeholder="Special Description" rows="2" cols="20" wrap="soft" class="description-text" id="descroiptionText${index}" value="${specialDescriptionLine || productDescription || ''}" data-prev="${productDescription}" name="${fieldValues[config.products.fields.descriptionName].value}">${specialDescriptionLine || productDescription || ''}</textarea>
            </div>
            <div class="info-container-row">
                <div class="info-container_column">
                    <div class="info-card-container">
                        <div>
                            <span id="aPriceLabel${index}" class="input-label">Price</span>
                            <input class="input-select-fields" placeholder="Price" class="price" id="price${index}" type="number" min="0" value="${price}" data-prev="${price}" name="${fieldValues[config.products.fields.descriptionName].value}">
                        </div>
                        <div>
                            <span class="input-label">Quantity</span>
                            <input class="input-select-fields" placeholder="Quantity" class="quantity" id="qty${index}" type="number" min="0" value="${qty}" data-prev="${qty}" name="${fieldValues[config.products.fields.descriptionName].value}">
                        </div>
                        <div>
                            <span id="coLabel${index}" class="input-label">CO %</span>
                            <input class="input-select-fields" placeholder="Commision Overwrite" class="commision" id="co${index}" type="number" ${hasItems ? '' : 'disabled'} value="${co}" min="0"">
                            <input style="display:none;" id="commissionPercent${index}" type="number" disabled value=""${commissionPercent} />
                        </div>
                        <div>
                            <span class="input-label">Type</span>
                            <input class="input-select-fields" placeholder="Enter Type" id="typeText${index}" type="text" value="${typeText}" data-prev="${typeText}" name="${fieldValues[config.products.fields.descriptionName].value}" />
                        </div>
                        <div>
                            <span class="input-label">Alternative Type</span>
                            <select class="input-select-fields select-field-custom" id="itemAlternativeType${index}" data-prev="Primary" style="width: 100%; height: 100%; border: none; background-image: url(&quot;data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjUiIHdpZHRoPSI3IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Ik0yLjgyOCA0LjM2NEwwIDEuNTM2LjcwNy44MjhsMi44MjkgMi44MjlMNi4zNjQuODI4bC43MDcuNzA4TDMuNTM2IDUuMDd6IiBmaWxsPSIjQkJCRUJGIi8+PC9zdmc+&quot;); background-position: right 10px center; background-repeat: no-repeat; border-radius: 2px; background-color: white;"  name="${fieldValues[config.products.fields.descriptionName].value}_type">
                                <option value="Primary" ${itemAlternativeType || itemAlternativeType === null ? 'selected' : ''}>Primary</option>
                                <option value="Alternative" ${itemAlternativeType === false ? 'selected' : ''}>Alternative</option>
                            </select>
                        </div>
                    </div>
                </div>
                <div class="info-container_column">
                    <div class="info-card-container">
                        <div>
                            <span class="input-label">A-Price</span>
                            <input class="input-select-fields" id="aPrice${index}" type="text" disabled  />
                        </div>
                        <div>
                            <span class="input-label">Hot-Price</span>
                            <input class="input-select-fields" id="hotPrice${index}" type="text" disabled  />
                        </div>
                        <div>
                            <span class="input-label">LifeCycle</span>
                            <input class="input-select-fields" id="lifeCycle${index}" type="text" disabled />
                        </div>
                        <div>
                            <span class="input-label">Item Type</span>
                            <input class="input-select-fields" id="itemType${index}" type="text" disabled />
                        </div>
                        <div>
                            <span class="input-label">Competitor</span>
                            <select class="input-select-fields select-field-custom" id="competitor${index}"  style="width: 100%; height: 100%; border: none; background-image: url(&quot;data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjUiIHdpZHRoPSI3IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Ik0yLjgyOCA0LjM2NEwwIDEuNTM2LjcwNy44MjhsMi44MjkgMi44MjlMNi4zNjQuODI4bC43MDcuNzA4TDMuNTM2IDUuMDd6IiBmaWxsPSIjQkJCRUJGIi8+PC9zdmc+&quot;); background-position: right 10px center; background-repeat: no-repeat; border-radius: 2px; background-color: white;">
                                <option value="" ${competitorName === '' || !competitorName ? 'selected' : ''}>None</option>
                                <option value="customCompetitor">Custom Competitor</option>
                                ${competitorOptions.map(option => `<option value="${option.field_values.name.display_value}" ${competitorName === option.field_values.name.display_value ? 'selected' : ''}>${option.field_values.name.display_value}</option>`)}
                            </select>
                        </div>
                    </div>
                </div>
                <div class="info-container_column">
                    <div class="info-card-container">
                        
                        <div>
                            <span class="input-label">Available</span>
                            <input class="input-select-fields" id="available${index}" type="text" disabled value="" />
                        </div>
                        <div>
                            <span class="input-label">Back Order</span>
                            <input class="input-select-fields" id="backOrder${index}" type="text" disabled value="" />
                        </div>
                        <div>
                            <span class="input-label">In Transit</span>
                            <input class="input-select-fields" id="inTransit${index}" type="text" disabled value="" />
                        </div>
                        <div>
                            <span class="input-label">Estimate Avail date</span>
                            <input class="input-select-fields" id="etaDate${index}" type="text" disabled />
                        </div>
                        <div>
                            <span class="input-label">Custom Competitor</span>
                            <input class="input-select-fields" id="customCompetitor${index}" type="text" disabled />
                        </div>
                    </div>
                </div>
                <div class="info-container_column">
                    <div class="info-card-container">
                        <div>
                            <span class="input-label">On Order</span>
                            <input class="input-select-fields" id="onOrder${index}" type="text" disabled value="" />
                        </div>
                        <div>
                            <span class="input-label">Reserved</span>
                            <input class="input-select-fields" id="reserved${index}" type="text" disabled value="" />
                        </div>
                        <div>
                            <span class="input-label">Allocated</span>
                            <input class="input-select-fields" id="allocated${index}" type="text" disabled value="" />
                        </div>
                        <div>
                            <span class="input-label">WH</span>
                            <select class="input-select-fields select-field-custom" id="wh${index}" value="${whLine || ''}" data-prev="" style="width: 100%; height: 100%; border: none; background-image: url(&quot;data:image/svg+xml;base64,PHN2ZyBoZWlnaHQ9IjUiIHdpZHRoPSI3IiB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciPjxwYXRoIGQ9Ik0yLjgyOCA0LjM2NEwwIDEuNTM2LjcwNy44MjhsMi44MjkgMi44MjlMNi4zNjQuODI4bC43MDcuNzA4TDMuNTM2IDUuMDd6IiBmaWxsPSIjQkJCRUJGIi8+PC9zdmc+&quot;); background-position: right 10px center; background-repeat: no-repeat; border-radius: 2px; background-color: white;"  name="${fieldValues[config.products.fields.descriptionName].value}_type">
                            </select>
                        </div>
                        <div id="inventoryState${index}">
                            <button class="inventory-details-button" style="padding: 1px 2px" disabled>Inventory Details</button>
                        </div>
                    </div>
                </div>
                <div class="info-container_buttons_column">
                    ${renderedButton()}
                </div>
            </div>
            <div class="alternate-selection-container-row" style="display: ${itemAlternativeType === false ? 'flex' : 'none'}">
                <span>Alternative to: </span>
                <select class="alternate-select"></select>
            </div>
            <div class="active-po-table-container" style="display: none">
            </div>
        </div>`;

        let qtyInput = productInfoContainer.querySelector(`#qty${index}`);
        let priceInput = productInfoContainer.querySelector(`#price${index}`);
        let commissionOverwriteInput = productInfoContainer.querySelector(`#co${index}`);
        let commissionPercentInput = productInfoContainer.querySelector(`#commissionPercent${index}`);
        let commissionLabel = productInfoContainer.querySelector(`#coLabel${index}`);
        let lifeCycleInput = productInfoContainer.querySelector(`#lifeCycle${index}`);
        let hotPriceInput = productInfoContainer.querySelector(`#hotPrice${index}`);
        let itemTypeInput = productInfoContainer.querySelector(`#itemType${index}`);
        let aPriceInput = productInfoContainer.querySelector(`#aPrice${index}`);
        let aPriceLabel = productInfoContainer.querySelector(`#aPriceLabel${index}`);
        let collapseButton = productInfoContainer.querySelector(`#collapseButton${index}`);
        let whField = productInfoContainer.querySelector(`#wh${index}`);
        let availableField = productInfoContainer.querySelector(`#available${index}`);
        let estimatedAvailableDateField = productInfoContainer.querySelector(`#etaDate${index}`);
        let backOrderField = productInfoContainer.querySelector(`#backOrder${index}`);
        let inTransitField = productInfoContainer.querySelector(`#inTransit${index}`);
        let onOrderField = productInfoContainer.querySelector(`#onOrder${index}`);
        let reservedField = productInfoContainer.querySelector(`#reserved${index}`);
        let allocatedField = productInfoContainer.querySelector(`#allocated${index}`);
        let alternativeTypeField = productInfoContainer.querySelector(`#itemAlternativeType${index}`);
        let alternateSelection = productInfoContainer.querySelector(`.alternate-select`);
        let alternateSelectionContainer = productInfoContainer.querySelector(`.alternate-selection-container-row`);
        let alternativeButton = productInfoContainer.querySelector(`#alternativeButton${index} > button`);
        let addButton = productInfoContainer.querySelector(`#addButton${index} > button`);
        let addButtonContainer = productInfoContainer.querySelector(`#addButton${index}`);
        let updateButton = productInfoContainer.querySelector(`#updateButton${index} > button`);
        let updateButtonContainer = productInfoContainer.querySelector(`#updateButton${index}`);
        let removeButton = productInfoContainer.querySelector(`#removeButton${index} > button`);
        let removeButtonContainer = productInfoContainer.querySelector(`#removeButton${index} `);
        let inventoryStateButton = productInfoContainer.querySelector(`#inventoryState${index} > button`);
        let inventoryStateButtonContainer = productInfoContainer.querySelector(`#inventoryState${index}`);
        let checkboxEl = productInfoContainer.querySelector(`#checkbox${index}`);
        let competitorField = productInfoContainer.querySelector(`#competitor${index}`);
        let customCompetitorInput = productInfoContainer.querySelector(`#customCompetitor${index}`);

        competitorField.addEventListener('change', (event) => {
            if (event.target.value === 'customCompetitor') {
                customCompetitorInput.removeAttribute('disabled');
            } else {
                customCompetitorInput.value = '';
                customCompetitorInput.setAttribute('disabled', true);
            }
        });

        alternativeTypeField.addEventListener('change', (event) => {
            alternateSelectionContainer.style.display = 'flex';
            alternateSelection.innerHTML = '';
            if (event.currentTarget.value === 'Alternative' && linesGlobal) {
                addAlternateToLines(alternateSelection, product, alternateToLine);
            } else {
                alternateSelectionContainer.style.display = 'none';
            }
        })

        priceInput.addEventListener("keyup", debounce(async (event) => {
            let qty = qtyInput.valueAsNumber;
            let unitPrice = priceInput.valueAsNumber;
            if(isNaN(qty)) {
                return
            }
            
            let isAssembly = itemTypeInput?.value === 'FG-A' || itemTypeInput?.value === "FG_A";
            
            if (isAssembly) {
                let eta = await getAssemblyItem(itemNumber, qty).catch(e => console.error(e));
                if (eta && eta.eta) {
                    estimatedAvailableDateField.value = formatDateFromStr(eta.eta);
                }
            }

            if (!executingRequest) {
                executingRequest = true;
                try {
                    let { commissionRate } = await getCommission(itemNumber, unitPrice, qty, repId).catch(e => console.error(e));
                    executingRequest = false;
                    addButton.disabled = false;
                    if (commissionRate || commissionRate === 0) {
                        commissionPercentInput.value = commissionRate.toFixed(2);
                        commissionOverwriteInput.value = commissionRate.toFixed(2);
                        commissionOverwriteInput.disabled = false;
                    } else {
                        if (itemNumber[0] === '#') {
                            if (addButton) {
                                addButton.disabled = false;
                            }
                        }
                    }
                } catch (e) {
                    console.error(e);
                    executingRequest = false;
                }
            }

        }, 1000))

        qtyInput.addEventListener("keyup", debounce(async (event) => {
            let unitPrice = priceInput.valueAsNumber;
            if(isNaN(unitPrice)) {
                return
            }
            let qty = qtyInput.valueAsNumber;
            let isAssembly = itemTypeInput?.value === 'FG-A' || itemTypeInput?.value === "FG_A";
            
            if (isAssembly) {
                let eta = await getAssemblyItem(itemNumber, qty).catch(e => console.error(e));
                if (eta && eta.eta) {
                    estimatedAvailableDateField.value = formatDateFromStr(eta.eta);
                }
            }

            if (!executingRequest) {
                executingRequest = true;
                try {
                    let { commissionRate } = await getCommission(itemNumber, unitPrice, qty, repId).catch(e => console.error(e));
                    executingRequest = false;
                    addButton.disabled = false;
                    if (commissionRate || commissionRate === 0) {
                        commissionPercentInput.value = commissionRate.toFixed(2);
                        commissionOverwriteInput.value = commissionRate.toFixed(2);
                        commissionOverwriteInput.disabled = false;
                    } else {
                        if (itemNumber[0] === '#') {
                            if (addButton) {
                                addButton.disabled = false;
                            }
                        }
                    }
                } catch (e) {
                    console.error(e);
                    executingRequest = false;
                }
            }

        }, 1000))

        alternativeButton?.addEventListener("click", async (event) => {
            searchSelectEl.value = 'Alternative';
            searchEl.value = itemNumber;
            await generateList({ products: null, isChange: true });
        });

        addButton?.addEventListener("click", (event) => {
            processLines(productInfoContainer, productDescription, product, index, 'add', itemNumber, null, repId).then(_ => {
                addButtonContainer.style.display = 'flex';
                updateButtonContainer.style.display = 'flex';
                removeButtonContainer.style.display = 'flex';
                loadingStart();
            })
                .catch(console.error);
        });

        updateButton?.addEventListener("click", (event) => {
            processLines(productInfoContainer, productDescription, product, index, 'update', itemNumber, lineId, repId).then(_ => {
                loadingStart();
            }).catch(console.error);;
        });

        removeButton?.addEventListener("click", (event) => {
            processLines(productInfoContainer, productDescription, product, index, 'remove', itemNumber, lineId).then(_ => {
                addButtonContainer.style.display = 'flex';
                updateButtonContainer.style.display = 'none';
                removeButtonContainer.style.display = 'none';
                loadingStart();
            }).catch(console.error);;
        });

        collapseButton.addEventListener('click', async (event) => {
            if (event.currentTarget && !event.currentTarget.classList.contains('collapsed')) {
                if (!itemAlternativeType) {
                    addAlternateToLines(alternateSelection, product, alternateToLine);
                }
                // let unitPrice = priceInput.valueAsNumber;
                // let qty = qtyInput.valueAsNumber;
                // let { commissionRate } = await getCommission(itemNumber, unitPrice, qty, repId).catch(e => console.error(e));
                // if (commissionRate || commissionRate === 0) {
                //     commissionPercentInput.value = commissionRate.toFixed(2);
                // }
                let itemResponse = await getItem(itemNumber);
                if (itemResponse) {
                    let { inventory, description, lifeCycle, prices, itemType } = itemResponse;
                    let [A, HOT] = prices;
                    lifeCycleInput.value = lifeCycle;
                    hotPriceInput.value = HOT.price;
                    aPriceInput.value = A.price;
                    itemTypeInput.value = itemType;
                    if (inventory) {
                        let availableTotal = 0;
                        let backOrderTotal = 0;
                        let inTransitTotal = 0;
                        let onOrderTotal = 0;
                        let reservedTotal = 0;
                        let allocatedTotal = 0;

                        inventory.map(({ available, backOrder, inTransit, onOrder, warehouseCd, reserved, allocated }) => {
                            if (available) {
                                availableTotal += available;
                            }
                            if (backOrder) {
                                backOrderTotal += backOrder;
                            }
                            if (inTransit) {
                                inTransitTotal += inTransit;
                            }
                            if (onOrder) {
                                onOrderTotal += onOrder;
                            }
                            if (reserved) {
                                reservedTotal += reserved;
                            }
                            if (allocated) {
                                allocatedTotal += allocated;
                            }
                        });

                        whField.innerHTML = '';
                        let totalOption = document.createElement('option');
                        totalOption.innerText = `All`;
                        totalOption.setAttribute('data-available', availableTotal);
                        totalOption.setAttribute('data-backorder', backOrderTotal);
                        totalOption.setAttribute('data-intransit', inTransitTotal);
                        totalOption.setAttribute('data-onorder', onOrderTotal);
                        totalOption.setAttribute('data-reserved', reservedTotal);
                        totalOption.setAttribute('data-allocated', allocatedTotal);
                        availableField.value = availableTotal;
                        backOrderField.value = backOrderTotal;
                        inTransitField.value = inTransitTotal;
                        onOrderField.value = onOrderTotal;
                        reservedField.value = reservedTotal;
                        allocatedField.value = allocatedTotal;
                        whField.appendChild(totalOption);
                        inventory.map(({ available, backOrder, inTransit, warehouseCd, onOrder, reserved, allocated }) => {
                            let optionDom = document.createElement('option');
                            optionDom.innerText = `${warehouseCd}`;
                            optionDom.setAttribute('data-available', available);
                            optionDom.setAttribute('data-intransit', inTransit);
                            optionDom.setAttribute('data-backorder', backOrder);
                            optionDom.setAttribute('data-onorder', onOrder);
                            optionDom.setAttribute('data-reserved', reserved);
                            optionDom.setAttribute('data-allocated', allocated);
                            whField.appendChild(optionDom);
                        })
                        whField.addEventListener('change', (event) => {
                            let {
                                available,
                                intransit,
                                backorder,
                                onorder,
                                reserved,
                                allocated
                            } = event.target.options[event.target.selectedIndex].dataset;
                            availableField.value = available;
                            backOrderField.value = intransit;
                            inTransitField.value = backorder;
                            onOrderField.value = onorder;
                            reservedField.value = reserved;
                            allocatedField.value = allocated;
                        });

                        inventoryStateButton.disabled = false;
            
                        inventoryStateButton.addEventListener('click', async (e) => {
                            let activePO = await getPurchaseOrders(itemNumber)
                            if (activePO && activePO.length > 0) {
                                
                                let activePORows = '';
                                activePO.map(({ mpoNo, mpoDate, warehouseCd, onOrder, inTransit, shippedDate, etaDate, memo }) => {
                                    
                                    activePORows += `<li class="active-po-row">
                                                        <span class="active-po-cell po-col1">${mpoNo}</span>
                                                        <span class="active-po-cell po-col2">${formatDateFromStr(mpoDate)}</span>
                                                        <span class="active-po-cell po-col3">${warehouseCd}</span>
                                                        <span class="active-po-cell po-col4">${onOrder}</span>
                                                        <span class="active-po-cell po-col5">${inTransit}</span>
                                                        <span class="active-po-cell po-col6">${formatDateFromStr(shippedDate)}</span>
                                                        <span class="active-po-cell po-col7">${formatDateFromStr(etaDate)}</span>
                                                        <span class="active-po-cell po-col8">${memo}</span>
                                                        </li>`
                                })
                                let activePoContainer = bodyContainer.querySelector(".active-po-container");
                                activePoContainer.style.display = 'flex';
                                activePoContainer.innerHTML = `   <div class="active-po-product-info-container">
                                                                            <div class="close-active-po-button">X</div>
                                                                            <div class="active-po-item-name"> <b style="white-space: pre-line"><span style="color: rgba(215, 217, 219, 1.00)"> (${itemNumber})</span> ${productName}</b></div>
                                                                            <div class="active-po-wh-info-container">
                                                                                <span class="input-label">LifeCycle: <b>${lifeCycle}</b></span>
                                                                                <span class="input-label">Available: <b>${availableTotal}</b></span>
                                                                                <span class="input-label">Back Order: <b>${backOrderTotal}</b></span>
                                                                                <span class="input-label">On Order: <b>${onOrderTotal}</b></span>
                                                                                <span class="input-label">In Transit: <b>${inTransitTotal}</b></span>
                                                                            </div>
                                                                        </div>
                                                                    <ul class="active-po-table-container">
                                                                        <li class="active-po-row header-row">
                                                                            <span class="active-po-cell po-col1">PoNo</span>
                                                                            <span class="active-po-cell po-col2">PoDate</span>
                                                                            <span class="active-po-cell po-col3">WH</span>
                                                                            <span class="active-po-cell po-col4">OnOrder</span>
                                                                            <span class="active-po-cell po-col5">InTransit</span>
                                                                            <span class="active-po-cell po-col6">ExFactoryDate</span>
                                                                            <span class="active-po-cell po-col7">ETA</span>
                                                                            <span class="active-po-cell po-col8">MEMO</span>
                                                                        </li>
                                                                        ${activePORows}
                                                                    </ul>
                                `;
                                activePoContainer.querySelector('.close-active-po-button').addEventListener('click', (e) => {
                                    activePoContainer.style.display = 'none';
                                    activePoContainer.innerHTML = '';
                                })
                            }
                        })
                        
                    }
                }
            }
        })

        liEl.appendChild(productInfoContainer);
        list.appendChild(liEl);
    });

}

async function generateUploadUI() {
    cleanList();

    let bodyContainer = document.querySelector('body');
    linesGlobal = await listLineItems();
    document.getElementById('reorder-lines-button').style.display = "none";

    let uploadContainer = bodyContainer.querySelector(".upload-container");
    let dropZoneContainer = uploadContainer.querySelector('.dropzone-container');
    uploadContainer.style.display = 'flex';
    dropZoneContainer.style.display = 'flex';

    uploadContainer.querySelector('.close-upload-button').addEventListener('click', (e) => {
        uploadContainer.style.display = 'none';
        uploadContainer.querySelector('#mxup-container').innerHTML = '';
        uploadContainer.querySelector('#mx-add-quote-container').style.display = 'none';
        bodyContainer.querySelector('#search-select').value = 'Search All';
        document.querySelector('.file-name-container').style.display = 'none';
        document.querySelector('#file-name').textContent = '';
        document.querySelector("#download-template-link").style.display = 'block';
    })

}

function downLoadQuoteCsv() {
    console.log('test')
    console.log(linesGlobal);
    quoteItems = linesGlobal.map(line => {
        let { field_values } = line
        return {
            ItemNo: field_values.quote_item_field0.display_value,
            Type: field_values.quote_item_field7.value == true ? 'Primary' : 'Alternative',
            Description: field_values.description.value ? field_values.description.value : field_values.quote_item_field22.value,
            Price: field_values.quote_item_field11.formatted_value,
            Quantity: field_values.quote_item_field14.value,
            LineAmount: field_values.quote_item_field12.formatted_value,
            CommissionPercent: field_values.quote_item_field18.formatted_value,
            CommissionAmount: field_values.quote_item_field13.formatted_value,
            APrice: field_values.quote_item_field30.formatted_value,
            HotPrice: field_values.quote_item_field31.formatted_value,
            ItemType: field_values.quote_item_field24.display_value
        }
    })
    let data = Papa.unparse(quoteItems)
    console.log('data',data,recordGlobal.seq_id)
   
    let csvData = new Blob([data]);
    const a = document.createElement('a');
    a.href = URL.createObjectURL(csvData, { type: 'text/csv;charset=utf-8;' });
    a.download = `${recordGlobal.seq_id}-quote-items.csv`;
    encodedUri = encodeURI(csvData)
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
 
}


function uploadCsv(file, myDropZone) {
    try {
        let uploadedItems = {};
        let data = Papa.parse(file, {
            skipEmptyLines: true,
            header: true,
            complete: async function (results) {
                let uploadContainer = document.querySelector(".upload-container");
                document.querySelector(".upload-container");
                uploadContainer.style.display = 'flex';
                document.querySelector("#download-template-link").style.display = 'none';
                document.querySelector('.file-name-container').style.display = 'flex';
                document.querySelector('#file-name').textContent = file.name;
                document.querySelector('#remove-selected-file').addEventListener('click', (e) => {
                    myDropZone.removeFile(file);
                    uploadContainer.querySelector('#mxup-container').innerHTML = '';
                    uploadContainer.querySelector('#mx-add-quote-container').style.display = 'none';
                    document.querySelector('.file-name-container').style.display = 'none';
                    document.querySelector('#file-name').textContent = '';
                    uploadContainer.querySelector('.dropzone-container').style.display = 'flex';
                    document.querySelector("#download-template-link").style.display = 'block';
                });
                let divEl = document.createElement('div')
                divEl.setAttribute('id', 'com-fetch-status')
                uploadContainer.append(divEl)
                let itemsWCommissions = await getBulkCommissions(results.data);
                uploadedItems = new MaxLiteUpload({ items: itemsWCommissions });
                let dataQualityCheck = uploadedItems.dataQualityCheck()
                if (dataQualityCheck[0]) {
                    uploadContainer.querySelector('#mx-add-quote-container').style.display = 'block';
                    let addToQuoteContainer = document.getElementById("mx-add-quote").style.display = 'block';
                    let mxupContainer = document.getElementById("mxup-container")
                    mxupContainer.innerHTML = uploadedItems.getItemsHtml();
                    let addToQuoteButton = document.getElementById("mx-add-quote");
                    let new_element = addToQuoteButton.cloneNode(true);
                    addToQuoteButton.parentNode.replaceChild(new_element, addToQuoteButton);
                    new_element.addEventListener('click', async (e) => {
                        loadingStart();
                        let mxItems = uploadedItems.getItemsFromHtml(linesGlobal)
                        let faItems = await getUploadProducts(mxItems)
                        let itemNos = mxItems.map((item) => {
                            return item.itemNumber
                        })
                        let faItemNos = faItems.map(item => {
                            return item.field_values.product_field0.display_value;
                        })
                        let skusNotFound = itemNos.filter(itemNo => {
                            return !faItemNos.includes(itemNo)
                        })
                        if (skusNotFound.length > 0) {
                            notyf.alert(`Skus Not Found: ${skusNotFound.join(',')}`)
                        }

                        let lineItems = uploadedItems.mergeWithFAData(mxItems, faItems)
                        let approvalsNeeded = uploadedItems.neededApprovals(lineItems)
                        await addUploadedItems(lineItems, approvalsNeeded);
                    })
                    uploadContainer.querySelector('.dropzone-container').style.display = 'none';
                    myDropZone.removeFile(file);
                } else {
                    uploadContainer.querySelector('#mx-add-quote-container').style.display = 'block';
                    document.getElementById("mx-add-quote").style.display = 'none';
                    let mxupContainer = document.getElementById("mxup-container")
                    mxupContainer.innerHTML = `${uploadedItems.getItemsHtml()}<div style="padding: 15px;color:white;background:red">${dataQualityCheck[1]} </div><p style="color:white">* Click "x" next to filename to try again</p>`;
                    uploadContainer.querySelector('.dropzone-container').style.display = 'none';
                    myDropZone.removeFile(file);
                }
            }
        });
    } catch (err) {
        console.log(err)
    }
}

function getUploadProducts(maxliteProducts) {
    let itemNos = maxliteProducts.map((item) => {
        return item.itemNumber
    })
    return FAClient.listEntityValues({
        entity: config.products.name,
        filters: [{ field_name: 'product_field0', operator: 'equals', values: itemNos }],
        order: [[config.products.fields.itemNumber, "ASC"]],
        limit: 250
    });
}

async function getBulkCommissions(items) {
    let itemsWCommissions = [];
    items = Array.isArray(items) ? items : [];
    let commisionStatus = document.getElementById('com-fetch-status');
    for (let i = 0; i < items.length; i++) {
        let item = items[i];

        if (!!!item['Rep Commission']) {
            let itemNumber = item["Item #"];
            let unitPrice = !!item["Unit Price"] ? item["Unit Price"].replace(/[^0-9.]/g, "") : 0;
            let quantity = !!item['Quantity'] ? item['Quantity'].replace(/[^0-9.]/g, "") : 0;
            let itemCommision = await getItemWCommission(itemNumber, parseFloat(unitPrice), parseInt(quantity), repId, item);
            commisionStatus.innerHTML = `<strong style="color: white;padding:5px">Adding Commission: ${i}/${items.length}</strong>`
            itemCommision = itemCommision == undefined ? '' : itemCommision
            itemsWCommissions.push(itemCommision)
        } else {
            itemsWCommissions.push(item);
        }
    }
    commisionStatus.innerHTML = ''
    return itemsWCommissions;
}

async function getItemWCommission(itemNo, unitPrice, qty, repNo = repId, item) {
    let { commissionRate } = await getCommission(itemNo, unitPrice, qty, repNo);
    commissionRate = commissionRate == undefined ? '' : commissionRate;
    return {
        ...item,
        ['Rep Commission']: commissionRate ? commissionRate.toFixed(2) : ''
    }
}

async function listItems(payload) {
    return await FAClient.listEntityValues(payload);
}

async function updateItem(payload) {
    return FAClient.updateEntity(payload);
}

async function upsertComposite(payload) {
    return FAClient.upsertCompositeEntity(payload);
}

async function updateLineItem(payload) {
    return FAClient.upsertCompositeEntity(payload);
}

async function showFaSuccessMessage(message = 'Success') {
    timer = 300;
    return FAClient.showSuccessMessage(message);
}

async function showFaErrorMessage(message) {
    return FAClient.showErrorMessage(message);
}

async function listLineItems() {
    return await FAClient.listEntityValues({
        entity: config.lines.name,
        filters: [
            {
                field_name: "parent_entity_reference_id",
                operator: "includes",
                values: [recordId],
            },
        ],
        limit: 1000
    });
}

async function deleteEntity(id) {
    let updatePayload = {
        entity: config.lines.name,
        id: id,
        field_values: { deleted: true }
    }
    updateItem(updatePayload).then(async data => {
        await showFaSuccessMessage('Removed successfully').catch(e => console.log(e));
        // await .catch(e => console.log(e));
        loadingEnd();
    }).catch(async e => await showFaErrorMessage())
    linesGlobal = await listLineItems();
}

async function updateQty({ id, qty = 1, price = '', co = '', commissionPercent = '', wh = '', type = true, alternativeTo, typeText, competitorValue, specialDescription, hotPrice, aPrice, lifeCycle, status, itemNumber}) {
    let updatePayload = {
        entity: config.lines.name,
        id: id,
        field_values: {
            [config.lines.fields.quantity]: qty,
            [config.lines.fields.price]: price,
            [config.lines.fields.commissionPercentOverwrite]: co,
            [config.lines.fields.commissionPercent]: commissionPercent,
            [config.lines.fields.wareHouse]: wh,
            [config.lines.fields.itemType]: type,
            [config.lines.fields.descriptionSpecial]: specialDescription,
            [config.lines.fields.alternativeTo]: alternativeTo,
            [config.lines.fields.typeText]: typeText,
            [config.lines.fields.competitorName]: competitorValue,
            [config.lines.fields.aPrice]: aPrice || 0,
            [config.lines.fields.hotPrice]: hotPrice || 0,
            [config.lines.fields.lifeCycle]: !!lifeCycle ? lifeCycle : null,
            [config.lines.fields.status]: !!status ? status : null
        },
    };
    console.log('itemNum',itemNumber)
    // updateItem(updatePayload).then(async data => {
    //     await showFaSuccessMessage('Updated successfully').catch(e => console.log(e));
    //     // await updateRecord().catch(e => console.log(e));
    //     loadingEnd();
    // }).catch(async e => await showFaErrorMessage());
    
    await upsertComposite(updatePayload).then(async data => {
        await showFaSuccessMessage('Updated successfully').catch(e => console.log(e));
        setQuoteToDraft()
        updateRecord()
        loadingEnd();
    }).catch(async e => await showFaErrorMessage());
    linesGlobal = await listLineItems();
    loadingStart()


    reRenderIncluded(itemNumber, price, qty)
    loadingEnd();
    console.log('generateList')

}

function reRenderIncluded(itemNo, price, qty) {
    
   
    try {
        const itemMeta = document.getElementById(`meta-${itemNo}`)
        if(itemMeta) {
            itemMeta.innerText = `
            Price: $${price}, Qty: ${qty}
            `
            itemMeta.parentNode.parentNode.parentNode.click()
        } else {
            console.log('guessnot')
        }
        
    } catch(e) {
        console.log(e)
    }
    return
}


async function addUploadedItems(itemLines, approvalsNeeded) {
    let promises = [];
    let loadingStatus = document.getElementById('up-cnt-status');
    let loadingCount = 0
    let notFounds = []
    loadingStatus.innerHTML = `<div><strong>Fetching Items: </strong> ${loadingCount}/${itemLines.length}</div>` 
    itemLines.forEach((item, index) => {
        promises.push(getItem(item.itemNo, item.order).then((item) => {
            let statusCell = document.getElementById(`up-status-${index}`)
            loadingCount++;
            if(statusCell) {
                statusCell.innerHTML = '<span style="padding: 5px;font-weight: bold;border-radius: 5px;background: green">Success</span>'
            }
            loadingStatus.innerHTML = `<div style="padding: 5x;color:white"><strong>Fetching Items: ${loadingCount}/${itemLines.length} </strong></div>` 
            return item
        }).catch(err => {
            notFounds.push(`<p style="color: white;background:red;padding: 7px;margin:5px;">Line: ${index+1}, ${item.itemNo} was not found in SM100</p>`)
        }));
    });

    let maxliteProducts = await Promise.all(promises);
    
    let createLines = {
        entity: config.parent.name,
        id: recordId,
        field_values: {
            [config.parent.fields.quoteStage]: "75a4351c-d78d-4a2f-a7dd-e22330f194de"
        },
        children: []
    }

    let [item, ...rest] = itemLines;
  
    itemLines.forEach((item) => {
        let missingItems = [];
        let maxliteProduct = null;
        try {
            maxliteProduct = maxliteProducts.find((m) => m.itemNo == item.itemNo && m.order == item.order);
            if(maxliteProduct == undefined || maxliteProduct == null) {
                missingItems.push(item.itemNo)
                console.log('undefined MaxliteProduct', item)
                return
            }
        } catch (e) {
            missingItems.push(item.itemNo);
            return
        }

        if (missingItems.length > 0) {
            showFaErrorMessage(`One or more items doesn't exist in Maxlite: ${missingItems.join()}`);
            return;
        }
       
        let aPrice = !!maxliteProduct.prices ? maxliteProduct.prices.find((p) => p.id == "A") : null;
        let hotPrice = !!maxliteProduct.prices ? maxliteProduct.prices.find((p) => p.id == "Hot") : null;
        let commissionPercentOverwrite = item.commissionPercentOverwrite && item.commissionPercentOverwrite !== '' ? parseInt(item.commissionPercentOverwrite, 10) : '';
        createLines.children.push({
            entity: config.lines.name,
            field_values: {
                [config.lines.fields.itemNumber]: item.itemNumber,
                [config.lines.fields.quantity]: parseInt(item.quantity || 0),
                [config.lines.fields.price]: parseFloat(item.price || 0),
                [config.lines.fields.commissionPercentOverwrite]: commissionPercentOverwrite,
                [config.lines.fields.itemType]: item.itemType,
                [config.lines.fields.alternativeTo]: item.alternativeTo,
                [config.lines.fields.order]: item.order || '',
                [config.lines.fields.typeText]: item.typeText,
                [config.lines.fields.lifeCycle]: maxliteProduct.lifeCycle,
                [config.lines.fields.aPrice]: aPrice ? aPrice.price : 0,
                [config.lines.fields.hotPrice]: hotPrice ? hotPrice.price : 0,
                [config.lines.fields.status]: '497567ca-3dbf-4c21-9eef-2fec2930d212'

            }
        })
    })
    loadingStatus.innerHTML = `<div style="padding: 5x;color:white"><strong>Saving to Freeagent... </strong></div>` 
    let results = await upsertComposite(createLines);
    console.log('upsertComplete')
    //need to do at least one itme seperately to trigger content reload for benefit of the user
    // let aPrice = !!item.prices ? item.prices.find((p) => p.id == "A") : null;
    // let hotPrice = !!item.prices ? item.prices.find((p) => p.id == "Hot") : null;
    // let commissionPercentOverwrite = item.commissionPercentOverwrite && item.commissionPercentOverwrite !== '' ? parseInt(item.commissionPercentOverwrite, 10) : '';
    // let upsertCompositeEntityPayload = {
    //     entity: config.lines.name,
    //     field_values: {
    //         [config.lines.fields.itemNumber]: item.itemNumber,
    //         [config.lines.fields.quantity]: parseInt(item.quantity || 0),
    //         [config.lines.fields.price]: parseFloat(item.price || 0),
    //         [config.lines.fields.commissionPercentOverwrite]: commissionPercentOverwrite,
    //         [config.lines.fields.itemType]: item.itemType,
    //         [config.lines.fields.alternativeTo]: item.alternativeTo,
    //         [config.lines.fields.order]: item.order || '',
    //         [config.lines.fields.typeText]: item.typeText,
    //         [config.lines.fields.lifeCycle]: item.lifeCycle,
    //         [config.lines.fields.aPrice]: aPrice ? aPrice.price : 0,
    //         [config.lines.fields.hotPrice]: hotPrice ? hotPrice.price : 0,
    //         [config.lines.fields.status]: '497567ca-3dbf-4c21-9eef-2fec2930d212',
    //         parent_entity_reference_id: recordId
    //     },
    //     children: []
    // }

    // //need to do at least one itme seperately to trigger content reload for benefit of the user
    // console.log(item)
    // await FAClient.upsertCompositeEntity(upsertCompositeEntityPayload)
   

    loadingEnd();
    if(notFounds.length) {
        loadingStatus.innerHTML = '<div style="padding: 5x;color:white"><strong>Done!</strong></div>' + notFounds.join('')
        await showFaSuccessMessage(`Added ${itemLines.length - notFounds.length}/${itemLines.length} items successfully`).catch(e => console.log(e));
    } else {
        loadingStatus.innerHTML = `<div style="padding: 5x;color:white"><strong>Done!</strong></div>` 
    }
    
   
    console.log('waiting LineItme')
    linesGlobal = await listLineItems();
    console.log('gotLineItme')
    document.querySelector('.close-upload-button').click()
}

async function addItem({ productId, qty = 1, price = '', co = '', commissionPercent = '', wh = '', type = true, alternativeTo, typeText, competitorValue, specialDescription = '', hotPrice, aPrice, lifeCycle, status }) {
    let createItem = {
        entity: config.parent.name,
        id: recordId,
        field_values: {
            [config.parent.fields.quoteStage]: "75a4351c-d78d-4a2f-a7dd-e22330f194de"
        },
        children: []
    }
    
    let payload = {
        entity: config.lines.name,
        field_values: {
            [config.lines.fields.itemNumber]: productId,
            [config.lines.fields.quantity]: qty,
            [config.lines.fields.price]: price,
            [config.lines.fields.commissionPercentOverwrite]: co,
            [config.lines.fields.commissionPercent]: commissionPercent,
            [config.lines.fields.wareHouse]: wh,
            [config.lines.fields.itemType]: type,
            [config.lines.fields.descriptionSpecial]: specialDescription,
            [config.lines.fields.alternativeTo]: alternativeTo,
            [config.lines.fields.typeText]: typeText,
            [config.lines.fields.competitorName]: competitorValue,
            parent_entity_reference_id: recordId,
            [config.lines.fields.aPrice]: aPrice || 0,
            [config.lines.fields.hotPrice]: hotPrice || 0,
            [config.lines.fields.lifeCycle]: !!lifeCycle ? lifeCycle : null,
            [config.lines.fields.status]: !!status ? status : "497567ca-3dbf-4c21-9eef-2fec2930d212",
            
        },
        children: []
    };
    
    let maxOrder = null
    
    try {
        maxOrder = linesGlobal.reduce((a,b) =>  {
            let aOrder = a.field_values[config.lines.fields.order]?.formatted_value
            aOrder = aOrder == null ? null : parseInt(aOrder)
       
            let bOrder = b.field_values[config.lines.fields.order]?.formatted_value
            bOrder = bOrder == null ? null : parseInt(bOrder)
       
            return aOrder > bOrder ? a : b
            
        })
        maxOrder = maxOrder.field_values[config.lines.fields.order]?.formatted_value
        
        if(maxOrder) {
            payload.field_values[[config.lines.fields.order]] = parseInt(maxOrder) + 1
        }
        
    } catch(err) {
        console.log('error', err)
    }
    createItem.children.push(payload)
    await upsertComposite(createItem).then(async data => {
        showFaSuccessMessage('Added successfully').catch(e => console.log(e));
        // updateRecord()
        // setQuoteToDraft()
        
    }).catch(async e => await showFaErrorMessage());
    loadingEnd();
    linesGlobal = await listLineItems();
    cleanList()
}

async function updateRecord(id = recordId,draft) {
    let updatePayload = {
        entity: config.parent.name,
        id: id,
        field_values: {},
        children: []
    }

    if(draft){
        updatePayload.field_values[config.parent.fields.quoteStage] = "75a4351c-d78d-4a2f-a7dd-e22330f194de";
    }
    await upsertComposite(updatePayload).catch(e => console.log(e));
}

async function setQuoteToDraft() {
    if(recordGlobal.field_values.quote_field37.value != "75a4351c-d78d-4a2f-a7dd-e22330f194de") {
        let updatePayload = {
            entity: config.parent.name,
            id: recordId,
            field_values: {},
            children: []
        }
        updatePayload.field_values[config.parent.fields.quoteStage] = "75a4351c-d78d-4a2f-a7dd-e22330f194de";
       const updatedRecord =  await upsertComposite(updatePayload).catch(e => console.log(e));
       console.log(updatedRecord)
    } else {
        console.log('Skip update record')
    }
}

function getFilteredProductList(productsToFilter, lines, value) {
    let linesObject = {};

    let includedProductsInOrder = [];

    if (value === 'Included') {
        lines.map(line => {
            let productFound = productsToFilter.find(prod => prod.id === line.field_values.quote_item_field0.value);
            if (productFound) {
                includedProductsInOrder.push(productFound);
            }
        })
        return includedProductsInOrder;
    } else {
        if (value && value !== 'All') {
            return searchList(productsToFilter, value)
        } else {
            return productList;
        }
    }

    return productsToFilter;
}

function searchList(listToSearch, value) {
    value = value?.toLowerCase();
    return listToSearch.filter(prod => {
        let nameMatch = prod?.field_values[config.products.fields.descriptionName]?.display_value?.toLowerCase().includes(value) || null;
        let itemNumber = prod?.field_values[config.products.fields.itemNumber]?.display_value?.toLowerCase().includes(value) || null;
        let subCategory = prod?.field_values[config.products.fields.subCategory]?.display_value?.toLowerCase().includes(value) || null;
        let mainCategory = prod?.field_values[config.products.fields.mainCategory]?.display_value?.toLowerCase().includes(value) || null;
        return nameMatch || itemNumber || subCategory || mainCategory;
    })
}

function addAlternateToLines(alternateSelection, product, alternateTo = null) {
    alternateSelection.innerHTML = '';
    linesGlobal.map(line => {
        let lineItemProdId = line?.field_values[config.lines.fields.itemNumber]?.value;
        let lineItemNum = line?.field_values[config.lines.fields.itemNumber]?.display_value;
        let lineItemDesc = line?.field_values[config.lines.fields.productName]?.display_value;
        if (lineItemProdId !== product.id) {
            let lineNameOption = document.createElement('option');
            lineNameOption.innerText = `(${lineItemNum}) ${lineItemDesc}`;
            lineNameOption.setAttribute('data-productid', lineItemProdId);
            lineNameOption.setAttribute('data-lineid', line.id);
            lineNameOption.setAttribute('data-itemnum', lineItemNum);
            lineNameOption.setAttribute('data-itemdesc', lineItemDesc);
            if (alternateTo === lineItemProdId) {
                lineNameOption.setAttribute('selected', true);
            }
            alternateSelection.appendChild(lineNameOption);
        }
    });
}


async function processLines(productInfoContainer, productDescription, product, index, operation, itemNumber, lineId, repId) {
    let qtyInput = productInfoContainer.querySelector(`#qty${index}`);
    let priceInput = productInfoContainer.querySelector(`#price${index}`);
    let commisionInput = productInfoContainer.querySelector(`#co${index}`);
    let commissionPercentInput = productInfoContainer.querySelector(`#commissionPercent${index}`);
    let whField = productInfoContainer.querySelector(`#wh${index}`);
    let typeField = productInfoContainer.querySelector(`#itemAlternativeType${index}`);
    let specialDescriptionField = productInfoContainer.querySelector(`#descroiptionText${index}`);
    let alternateSelection = productInfoContainer.querySelector(`.alternate-select`);
    let typeTextField = productInfoContainer.querySelector(`#typeText${index}`);
    let competitorField = productInfoContainer.querySelector(`#competitor${index}`);
    let customCompetitorInput = productInfoContainer.querySelector(`#customCompetitor${index}`);
    let lifeCycle = productInfoContainer.querySelector(`#lifeCycle${index}`);
    let aPrice = productInfoContainer.querySelector(`#aPrice${index}`);
    let hotPrice = productInfoContainer.querySelector(`#hotPrice${index}`);

    let qty = qtyInput.valueAsNumber;
    let price = priceInput.valueAsNumber;
    let isValid = false;
    let itemHasHash = itemNumber?.startsWith('#', 0);

    if (operation === 'add') {
        if ((price && price > 0 && qty && qty > 0) || itemHasHash) {
            isValid = true;
        } else {
            if (!price || price <= 0 || price === '') {
                priceInput.style.backgroundColor = 'rgba(242, 23, 22, 0.8)';
                setTimeout(() => { priceInput.style.backgroundColor = 'rgba(255,255,255,1)' }, 1000);
            }
            if (!qty || qty <= 0 || qty === '') {
                qtyInput.style.backgroundColor = 'rgba(242, 23, 22, 0.8)';
                setTimeout(() => { qtyInput.style.backgroundColor = 'rgba(255,255,255,1)' }, 1000);
            }
        }
        if (isValid) {
            let commissionPercent = commissionPercentInput.valueAsNumber;
            let { commissionRate } = await getCommission(itemNumber, price, qty, repId).catch(e => console.error(e));
            if (commissionRate || commissionRate === 0) {
                commissionPercentInput.value = commissionRate.toFixed(2);
            }
            let co = commisionInput.valueAsNumber;
            let wh = whField.value;
            let isPrimary = !!typeField && typeField.value === 'Primary';
            let alternativeTo = !isPrimary ? alternateSelection?.options[alternateSelection.selectedIndex]?.dataset?.productid : '';
            let typeText = typeTextField.value;
            let specialDescription = specialDescriptionField.value !== productDescription ? specialDescriptionField.value : '';
            let competitorValue = competitorField?.value && competitorField?.value !== 'customCompetitor' ? competitorField?.value : customCompetitorInput?.value ? customCompetitorInput.value.trim() : '';

            let lineFound = linesGlobal?.find(line => line.field_values[config.lines.fields.itemNumber].value === product.id);

            if (qty > 0 || itemHasHash) {
                let specialDescription = specialDescriptionField.value !== productDescription ? specialDescriptionField.value : '';
                addItem({
                    productId: product.id,
                    qty,
                    price,
                    co,
                    commissionPercent,
                    wh,
                    type: isPrimary,
                    alternativeTo,
                    typeText,
                    competitorValue,
                    specialDescription,
                    hotPrice: hotPrice.value,
                    aPrice: aPrice.value,
                    lifeCycle: lifeCycle.value,
                    status: '497567ca-3dbf-4c21-9eef-2fec2930d212'
                }).then(res => null).catch(e => console.log(e));
                document.querySelector(`#liEl${index}`).classList.add('included-in-lines')
                qtyInput.dataset.prev = qty;
            }
            /* } */
        } else {
            if (qty > 0 || itemHasHash) {
                let specialDescription = specialDescriptionField.value !== productDescription ? specialDescriptionField.value : '';
                addItem({
                    productId: product.id,
                    qty,
                    price,
                    co,
                    commissionPercent,
                    wh,
                    type: isPrimary,
                    alternativeTo,
                    typeText,
                    competitorValue,
                    specialDescription,
                    hotPrice: hotPrice.value,
                    aPrice: aPrice.value,
                    lifeCycle: lifeCycle.value,
                    status: '497567ca-3dbf-4c21-9eef-2fec2930d212'
                }).then(res => null).catch(e => console.log(e));
                document.querySelector(`#liEl${index}`).classList.add('included-in-lines')
                qtyInput.dataset.prev = qty;
            }
        }
    }

    if (operation === 'update' || operation === 'remove') {
        let co = commisionInput.valueAsNumber;
        let commissionPercent = commissionPercentInput.valueAsNumber;
        let wh = whField.value;
        let isPrimary = !!typeField && typeField.value === 'Primary';
        let alternativeTo = !isPrimary ? alternateSelection?.options[alternateSelection.selectedIndex]?.dataset?.productid : '';
        let typeText = typeTextField.value;
        let specialDescription = specialDescriptionField.value !== productDescription ? specialDescriptionField.value : '';
        let competitorValue = competitorField?.value && competitorField?.value !== 'customCompetitor' ? competitorField?.value : customCompetitorInput?.value ? customCompetitorInput.value.trim() : '';

        let lineFound = linesGlobal.find(line => line.id === lineId);
        let qtyLine = lineFound.field_values.quote_item_field14.value;
        let priceLine = lineFound.field_values.quote_item_field11.value;
        let itemStatus = lineFound.field_values.quote_item_field34.value;

        if (lineFound) {
            if (qty > 0 && operation !== 'remove') {
                if (price && price !== '' && price > 0) {
                    loadingStart()
                    updateQty({
                            id: lineFound.id,
                            qty,
                            price,
                            co,
                            commissionPercent,
                            wh,
                            type: isPrimary,
                            alternativeTo, 
                            typeText,
                            competitorValue,
                            specialDescription,
                            hotPrice: hotPrice.value,
                            aPrice: aPrice.value,
                            lifeCycle: lifeCycle.value,
                            status: qty != qtyLine || price != priceLine ? '497567ca-3dbf-4c21-9eef-2fec2930d212' : itemStatus,
                            itemNumber
                        }).then(res => console.log(res)).catch(e => console.log(e));
                    qtyInput.dataset.prev = qty;
                    // if(recordGlobal.field_values.quote_field37.value != "75a4351c-d78d-4a2f-a7dd-e22330f194de") {
                    //     let draft = qty != qtyLine || price != priceLine || itemStatus == "497567ca-3dbf-4c21-9eef-2fec2930d212"
                    //     if(draft) {
                    //         setQuoteToDraft()
                    //         updateRecord()
                    //     }                   
                    //     loadingEnd()
                    // } else {
                    //     console.log('skip update')
                    // }
                }
            }
            if (qty === 0 || operation === 'remove') {
                deleteEntity(lineFound.id).then(res => null).catch(e => console.log(e))
                updateRecord()
                linesGlobal = linesGlobal.find(line => line.id === lineId);
                lineFound = null;
                let liElement = document.querySelector(`#liEl${index}`);
                if (document.getElementById('search-select')?.value === 'Included') {
                    liElement.remove();
                } else {
                    liElement.classList.remove('included-in-lines');
                    qtyInput.dataset.prev = '0';
                    priceInput.value = 0;
                    qtyInput.value = 0;
                    typeTextField.value = '';
                    alternateSelection.value = 'Primary';
                }
            }
        }
    }

}


function renderedReorderLines(lines) {
    let orderList = document.querySelector('#order-list');
    let innerHTML = ``;
    lines.map(({ field_values, id }, index) => {
        const num = field_values?.quote_item_field0?.display_value;
        let quantity = field_values?.quote_item_field14.display_value;
        let price = field_values?.quote_item_field11.display_value;
        let description = field_values?.quote_item_field22?.display_value;
        innerHTML += `<li class="reorder-element" style="padding:5px;color: white" data-id="${id}"><b style="white-space: pre-line"><span style="color: rgba(215, 217, 219, 1.00);padding: 5px;background:green"> ${num}</span> <span>Price: ${price}, Qty: ${quantity}</span><br> <p>${description}</p></b></li>`;
    })
    orderList.innerHTML = innerHTML;
    $("#order-list").sortable({
        change: function (event, ui) {
            let children = document.querySelector('#order-list').childNodes;
            for (let i = 0; i < children.length; i++) {
                if (children[i].dataset.id) {
                    console.log(children[i]);
                }
            }
        }
    });
    $("#order-list").disableSelection();
    return null;
}

async function saveReorderedLine(button) {
    loadingStart();
    document.getElementById('reorder-lines-button').style.display = 'none';
    let children = document.querySelector('#order-list').childNodes;

    let childrenPayload = [];

    for (let i = 0; i < children.length; i++) {
        let lineId = children[i]?.dataset?.id;
        if (lineId) {

            let lineMatch = linesGlobal.find(line => line.id === lineId);
            if (lineMatch) {
                let upsertCompositeEntityPayload = {
                    entity: config.lines.name,
                    id: lineId,
                    field_values: { [config.lines.fields.order]: i + 1 },
                    children: []
                }
                childrenPayload.unshift(upsertComposite(upsertCompositeEntityPayload));
            }
        }
    }

    await Promise.all(childrenPayload)

    linesGlobal = await listLineItems();
    await generateList({ isChange: true }).catch(e => console.log(e));

    loadingEnd();
    document.getElementById('reorder-lines-button').style.display = 'flex';
    return null;
}

function renderedDeleteLines(lines) {
    deleteLinesGlobal = [];
    let orderList = document.querySelector('#order-list');
    orderList.innerHTML = '';
    let innerHTML = ``;
    lines.map(({ field_values, id }, index) => {
        let num = field_values?.quote_item_field0?.display_value;
        let description = field_values?.quote_item_field22?.display_value;
        let liDeleteEl = document.createElement('li');
        liDeleteEl.setAttribute('class', 'delete-element');
        liDeleteEl.setAttribute('data-id', id);
        liDeleteEl.style.color = "white";
        liDeleteEl.innerHTML = `<b style="white-space: pre-line"><span style="color: rgba(215, 217, 219, 1.00)"> (${num})</span> ${description} <span><img src="height_white_18dp.svg" alt=""/></span></b>`;
        liDeleteEl.addEventListener('click', (e) => {
            if (deleteLinesGlobal.includes(e.currentTarget.dataset.id)) {
                deleteLinesGlobal = deleteLinesGlobal.filter(item => item !== e.currentTarget.dataset.id);
                liDeleteEl.style.opacity = '1';
            } else {
                deleteLinesGlobal.push(e.currentTarget.dataset.id);
                liDeleteEl.style.opacity = '0.4';
            }

        })
        orderList.appendChild(liDeleteEl);
        //innerHTML += `<li class="reorder-element" id=delete_line${index} style="color: white" data-id="${id}"><b style="white-space: pre-line"><span style="color: rgba(215, 217, 219, 1.00)"> (${num})</span> ${description} <span><img src="height_white_18dp.svg" alt=""/></span></b></li>`;
    })
    //orderList.innerHTML = innerHTML;
    return null;
}

async function bulkDeleteLines(linesToDelete = deleteLinesGlobal) {
    loadingStart();
    let deletePromises = [];

    linesToDelete.map(lineId => {
        let payload = {
            entity: config.lines.name,
            id: lineId,
            field_values: { deleted: true }
        }
        deletePromises.push(FAClient.updateEntity(payload));
    })
    if (deletePromises.length > 0) {
        try {
            await Promise.all(deletePromises);
            await updateRecord();
            linesGlobal = await listLineItems();
            await generateList({ products: null, isChange: true })
        } catch (e) {
            notyf.alert(e.message);
        }
    }
    loadingEnd();
}

function getLineFields(line, id = null, recordId = null) {
    let productId = line?.field_values[config.lines.fields.itemNumber]?.value || '';
    let qty = line?.field_values[config.lines.fields.quantity]?.value || '';
    let price = line?.field_values[config.lines.fields.price]?.value || '';
    let co = line?.field_values[config.lines.fields.commissionPercentOverwrite]?.value || '';
    let whLine = line?.field_values[config.lines.fields.wareHouse]?.value || '';
    let itemType = line?.field_values[config.lines.fields.itemType]?.value || true;
    let alternateToLine = line?.field_values[config.lines.fields.alternativeTo]?.value || null;
    let typeText = line?.field_values[config.lines.fields.typeText]?.value || '';
    let specialDescriptionLine = line?.field_values[config.lines.fields.descriptionSpecial]?.value || null;
    let competitorName = line?.field_values[config.lines.fields.competitorName]?.display_value || '';
    let payload = {
        entity: config.lines.name,
        id,
        field_values: {
            [config.lines.fields.itemNumber]: productId,
            [config.lines.fields.quantity]: qty,
            [config.lines.fields.price]: price,
            [config.lines.fields.commissionPercentOverwrite]: co,
            [config.lines.fields.wareHouse]: whLine,
            [config.lines.fields.itemType]: itemType,
            [config.lines.fields.descriptionSpecial]: specialDescriptionLine,
            [config.lines.fields.alternativeTo]: alternateToLine,
            [config.lines.fields.typeText]: typeText,
            [config.lines.fields.competitorName]: competitorName,
        }
    }

    if (recordId) {
        payload.field_values.parent_entity_reference_id = recordId;
    }

    return payload;
}

async function postData(url = '', data = {}) {
    const response = await fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify(data)
    });
    return response.json();
}

let headers = {
    "MX-Api-Key": "",
    "accept": "text/plain"
}

async function getItem(itemNo, order=null) {
    headers['MX-Api-Key'] = keyGlobal;
    let url = `${baseUrlGlobal}/items?itemNo=${encodeURIComponent(itemNo)}`
    let response = await fetch(url, {
        method: 'GET',
        headers: headers,
    });
    
    if (response.ok) {
        let item = await response.json()
        console.log(item)
        return {...item, order}
    } else {
        //try  one more time, maxlite api has been periodically returning 404 or res not ok, and on second attempt it works
        //will ask MaxLite IT Team why
        console.log('recovering')
        let recover = await fetch(url, {
            method: 'GET',
            headers: headers,
        });
        let recoveredItem = recover.json()
        if(recover.ok || 'itemNo' in recoveredItem) {
            console.log('recovery successful')
            return {...recoveredItem, order}
        } 
        notyf.alert(`${itemNo} haven't been found in SM100`);
    }
    return null;
}

async function getCommission(itemNo, unitPrice = 50.5, qty = 100, repNo = repId) {
    let url = `${baseUrlGlobal}/items/commission?itemNo=${itemNo}&unitPrice=${unitPrice}&qty=${qty}&repNo=${repNo}`;
    headers['MX-Api-Key'] = keyGlobal;
    let response = await fetch(url, {
        method: 'GET',
        headers,
    });
    if (response && response.ok) {
        return response.json();
    } else {

        console.log(`Failed to get commission from SM100`);
    }
    return { commissionRate: null };
}

async function getMargin({ commission, type, commissionAmount, description, item, lifeCycle, lineTotal, price, quantity, primary}) {
    let url = `${baseUrlGlobal}/items?itemNo=${item}`;
    headers['MX-Api-Key'] = keyGlobal;
    let response = await fetch(url, {
        method: 'GET',
        headers,
    });
    if (response && response.ok) {
        const item = await response.json();
        console.log('item',item)
        let aPrice = null;
        let hotPrice = null;
        let margin = null;
        let marginAfterComission = null;
        let gp = null;
        let gpAfterComission = null;
        if(item.prices) {
            aPrice = item.prices.find(price => price.id == "A") != undefined ? item.prices.find(price => price.id == "A").price : null
            hotPrice = item.prices.find(price => price.id == "Hot") != undefined ? item.prices.find(price => price.id == "Hot").price : null
            
            margin = (price - item.standardCost) * quantity;
            margin = new Intl.NumberFormat("en-US", {
                style: "currency",
                currency: "USD"
            }).format(margin);
            gp = (  (price - item.standardCost) / price);
            gp = new Intl.NumberFormat("en-US", {
                style: "percent",
                maximumFractionDigits: 2
            }).format(gp);
            marginAfterComission = (price - item.standardCost - (price * commission / 100)) * quantity;
            marginAfterComission = new Intl.NumberFormat("en-US", {
                style: "currency",
                currency: "USD"
            }).format(marginAfterComission);
            gpAfterComission = (  (price - item.standardCost - (price * commission / 100)) / price);
            gpAfterComission = new Intl.NumberFormat("en-US", {
                style: "percent",
                maximumFractionDigits: 2
            }).format(gpAfterComission);
        }
        return {
            'Item No': item.itemNo,
            'Qty.': quantity,
            'Quoted Price': new Intl.NumberFormat("en-US", {
                style: "currency",
                currency: "USD"
            }).format(price),
            'Standard Cost': new Intl.NumberFormat("en-US", {
                style: "currency",
                currency: "USD"
            }).format(item.standardCost),
            'A Price': new Intl.NumberFormat("en-US", {
                style: "currency",
                currency: "USD"
            }).format(aPrice),
            'Hot Price': new Intl.NumberFormat("en-US", {
                style: "currency",
                currency: "USD"
            }).format(hotPrice),
            'LC': lifeCycle,
            'Item Type': item.itemType,
            'Type': type,
            'Primary': primary,
            'Description': description,
            'Commission':new Intl.NumberFormat("en-US", {
                style: "percent",
                maximumFractionDigits: 2
            }).format(commission),
            'Commission Amount': new Intl.NumberFormat("en-US", {
                style: "currency",
                currency: "USD"
            }).format(commissionAmount),
            'Line Total': new Intl.NumberFormat("en-US", {
                style: "currency",
                currency: "USD"
            }).format(lineTotal),
            'Margin': margin,
            'GP': gp,
            'Margin After Commission': marginAfterComission,
            'GP After Commission': gpAfterComission
        }
    } else {

        console.log(`Failed to get Margin`);
    }
    return {
        'Item No': item,
        'Qty.': quantity,
        'Quoted Price': new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD"
        }).format(price),
        'Standard Cost': null,
        'A Price': null,
        'Hot Price': null,
        'LC': lifeCycle,
        'Item Type': '',
        'Type': type,
        'Primary': primary,
        'Description': description,
        'Commission':new Intl.NumberFormat("en-US", {
            style: "percent",
            maximumFractionDigits: 2
        }).format(commission),
        'Commission Amount': new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD"
        }).format(commissionAmount),
        'Line Total': new Intl.NumberFormat("en-US", {
            style: "currency",
            currency: "USD"
        }).format(lineTotal),
        'Margin': null,
        'GP': null
    }
}
async function getPurchaseOrders(itemNo) {
    let url = `${baseUrlGlobal}/purchase-orders?itemNo=${itemNo}`;
    headers['MX-Api-Key'] = keyGlobal;
    let response = await fetch(url, {
        method: 'GET',
        headers,
    });
    if (response && response.ok) {
        return response.json();
    } else {
        console.log(`No purchase orders from SM100`);
        notyf.alert('No purchase orders from SM100')
    }
    return null;
}

async function getAssemblyItem(itemNo, qty = 100) {
    let url = `${baseUrlGlobal}/items/assembly-eta?itemNo=${itemNo}&qty=${qty}`;
    headers['MX-Api-Key'] = keyGlobal;
    let response = await fetch(url, {
        method: 'GET',
        headers,
    });
    if (response && response.ok) {
        return response.json();
    } else {
        console.log(`Failed to get assembly item info from SM100`);
    }
    return null;
}

function formatDateFromStr(str) {
    if (str?.match(/[0-9]{8}/g)) {
        let year = `${str[0]}${str[1]}${str[2]}${str[3]}`
        let month = `${str[4]}${str[5]}`;
        let date = `${str[6]}${str[7]}`;
        return `${year}/${month}/${date}`
    } else {
        return '';
    }
}

function loadingStart() {
    document.getElementById('global-loading').style.display = "block";
}

function loadingEnd() {
    document.getElementById('global-loading').style.display = "none";
}

function extractNumberFromDollarString(dollarString) {
    const regex = /\$([0-9,]+(\.[0-9]{2})?)/;
    const match = dollarString.match(regex);
  
    if (match) {
      const numberString = match[1].replace(/,/g, ''); // Remove commas
      return parseFloat(numberString);
    }
  
    return 0; // Return 0 if no valid match is found
  }

function handleSum(marginLines) {
    let toalCommissionAmount = 0;
    let totalLineTotal = 0;
    let totalMargin = 0;
    let totalMarginAfterCommission = 0;
    let gp = null;
    let gpAfterComission = null;

    marginLines.map((line) => {
        toalCommissionAmount += extractNumberFromDollarString(line['Commission Amount']);
        totalLineTotal += extractNumberFromDollarString(line['Line Total']);
        totalMargin += extractNumberFromDollarString(line.Margin);
        totalMarginAfterCommission += extractNumberFromDollarString(line['Margin After Commission']);
    });

    if (totalLineTotal) {
        gp = new Intl.NumberFormat("en-US", {
            style: "percent",
            maximumFractionDigits: 2
        }).format(totalMargin / totalLineTotal);

        gpAfterComission = new Intl.NumberFormat("en-US", {
            style: "percent",
            maximumFractionDigits: 2
        }).format(totalMarginAfterCommission / totalLineTotal);
    }

    toalCommissionAmount = new Intl.NumberFormat("en-US", {
        style: "currency",
        currency: "USD"
    }).format(toalCommissionAmount);

    totalLineTotal = new Intl.NumberFormat("en-US", {
        style: "currency",
        currency: "USD"
    }).format(totalLineTotal);

    totalMargin = new Intl.NumberFormat("en-US", {
        style: "currency",
        currency: "USD"
    }).format(totalMargin);

    totalMarginAfterCommission = new Intl.NumberFormat("en-US", {
        style: "currency",
        currency: "USD"
    }).format(totalMarginAfterCommission);

    marginLines.push({
        'Item No': null,
        'Qty.': null,
        'Quoted Price': null,
        'Standard Cost': null,
        'A Price': null,
        'Hot Price': null,
        'LC': null,
        'Item Type': null,
        'Type': null,
        'Primary': null,
        'Description': null,
        'Commission':null,
        'Commission Amount': toalCommissionAmount,
        'Line Total': totalLineTotal,
        'Margin': totalMargin,
        'GP': gp,
        'Margin After Commission': totalMarginAfterCommission,
        'GP After Commission': gpAfterComission
    });
}